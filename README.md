# db-provisioning

Project for repeatable database clusters, https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/492

## Issue creation

Please create new issues in the [public infrastructure project](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/) and not in this private issue tracker.

## Creating a new Shard (Database Cluster)

Note we are still in the very early stages of creating new Database clusters in this Project:

1. Create a new branch.
1. Update `shards.libsonnet` to include the name of the new shard and the environments it should be deployed to
1. Run `make generate-config`
1. After pushing the change, on the ops pipeline confirm that you see the plan results.

## Secrets

Secrets are set in [Google Secret Manager](https://cloud.google.com/secret-manager). Secrets are fetched from GSM using a custom lookup plugin, accessing them requires `GOOGLE_APPLICATION_CREDENTIALS` to be set in the environment.

### Placeholders that should be updated after initial Terraform run

#### Consul

By default encryption and TLS is enabled for Consul, though if you do not update the placeholder key for `consul_encrypt` it won't be enabled in the GitLab configuration.

On an existing consul server issue the following commands:

```
consul keygen > consul.encryption.key
consul tls ca create
consul tls cert create -server -dc gitlab_consul
consul tls cert create -client -dc=gitlab_consul
```

The resulting files will need to be saved as existing GSM secrets:

- `consul_encrypt`: Contents of `consul.encryption.key`
- `consul_ca`: Contents of `consul-agent-ca.pem`
- `consul_server_cert`: Contents of `gitlab_consul-server-consul-0.pem`
- `consul_server_key`: Contents of `gitlab_consul-server-consul-0-key.pem`
- `consul_client_cert`: Contents of `gitlab_consul-client-consul-0.pem`
- `consul_client_key`: Contents of `gitlab_consul-client-consul-0-key.pem`

The following commands would be used to update all consul secrets in an environment if they haven't yet been set:

```
project=<project>
env=<env>
gcloud --project "$project" secrets versions add "$env-consul_encrypt" --data-file=consul.encryption.key
gcloud --project "$project" secrets versions add "$env-consul_ca" --data-file=consul-agent-ca.pem
gcloud --project "$project" secrets versions add "$env-consul_server_cert" --data-file=gitlab_consul-server-consul-0.pem
gcloud --project "$project" secrets versions add "$env-consul_server_key" --data-file=gitlab_consul-server-consul-0-key.pem
gcloud --project "$project" secrets versions add "$env-consul_client_cert" --data-file=gitlab_consul-client-consul-0.pem
gcloud --project "$project" secrets versions add "$env-consul_client_key" --data-file=gitlab_consul-client-consul-0-key.pem
```

### Adding a new secret

- Before adding a new secret you will need to seed the secret in Terraform/Ansible, add the secret to `shard` configuration in `shards.libsonnet`.
- Run `make generate-config`
- Commit the changes and run Terraform in CI to create the new secret which will be seeded with a random string

### Modifying an existing secret

By default, Ansible will lookup the latest secret version. To rotate or modify a secret you will need to create a new secret version.

- Navigate to the [secret manager in the google console](https://console.cloud.google.com/security/secret-manager) for the corresponding project or do the following on the commandline:

```
gcloud --project <project> secrets versions add <secret-id> --data-file="/path/to/file.txt"

```

For the secret ID, see the lookup in `ansible/environments/<env>/shards/<name>/inventory/vars.yml`

## Sandbox Environments

There are 4 dedicated Sandbox environments to allow for multiple SREs to work on Ansible/Terraform configuration without stepping on each other.
Once the project is stable, we will likely remove the alpha, beta, gamma envs. The current assignments are:

- `sb-alpha`: @jarv
- `sb-beta`: @alejandro
- `sb-gamma`: @ahmadsherif

## Creating a new environment

### Terraform

- Create [a new environment on ops.gitlab.net](https://ops.gitlab.net/gitlab-com/gl-infra/ansible-workloads/db-provisioning/-/environments)
- Create a new project using [env-projects](https://ops.gitlab.net/gitlab-com/gitlab-com-infrastructure/-/blob/master/environments/env-projects/main.tf) this will create the `terraform-ci` service account
- Generate a new `.json` key for the service account

```
cd /path/to/db-provisioning/private
project=<name of project>
gcloud --project $project iam service-accounts keys create $project.json --iam-account=terraform-ci@$project.iam.gserviceaccount.com
```

- Add it as a CI variable `GOOGLE_APPLICATION_CREDENTIALS` to https://ops.gitlab.net/gitlab-com/gl-infra/ansible-workloads/db-provisioning/-/settings/ci_cd for the corresponding environment name.
- Run Terraform in the CI pipeline to create the resources for the environment

### Ansible

Ansible requires an ssh key to access the infrastructure, the keys are associated with the `terraform-ci` service account, which is created in [env-projects](https://ops.gitlab.net/gitlab-com/gitlab-com-infrastructure/-/blob/master/environments/env-projects).
Before configuration can run you will need to associate an SSH key to the `terraform-ci` service account.

- Set the project name associated with the environment

```
project=<project for environment>
```

- Create the ssh key pair

```
ssh-keygen -t ed25519 -f $project-ssh-key-ansible-sa
```

- Add the SSH key for OS login

```
 gcloud --project $project auth activate-service-account --key-file=$project.json
 gcloud --project $project compute os-login ssh-keys add --key-file=$project-ssh-key-ansible-sa.pub
```

- Save the key files in the `db-provisioning/private` directory
- Add the private key as a CI variable `ANSIBLE_SSH_PRIVATE_KEY_FILE` to https://ops.gitlab.net/gitlab-com/gl-infra/ansible-workloads/db-provisioning/-/settings/ci_cd for the corresponding environment name.

- Get the unique service account ID for login

```
id=$(gcloud --project $project iam service-accounts describe terraform-ci@$project.iam.gserviceaccount.com --format='value(uniqueId)')
```

- The ssh username will be `sa_$id`

### Configuration files

- Update `shards.libsonnet` to include the new environments. The `ansible_user` should be set to the ssh username obtained in the previous step.
- Run `make generate-config`
- Link the terraform common files to the new environment folder:

```
cd terraform/environments/$env
ln -s ../common/*.tf .
```

### CI Environment Variables

The following are the environment variables required for CI to properly interact with an environment:

* `ANSIBLE_SSH_PRIVATE_KEY_FILE`: Environment-specific. SSH key for the Ansible user. See above for how to generate.
* `GOOGLE_APPLICATION_CREDENTIALS`: Environment-specific. GCP credentials required to list available machines among other things. See above for how to generate.
* `KNIFE_RB_FILE`: A configuration file for Chef's knife. Used currently to list items from the `users` data bag.
    * Attributes required in the configuration file:
      * `node_name`: Set to `db-provisioning-ansible`
      * `client_key`: Set to `client.pem`. CI (in `.gitlab-ci.yml`) will place the file in the correct directory.
      * `chef_server_url`: Set to the URL of the `gitlab` organization.
* `CHEF_KEY_FILE`: The private key of `db-provisioning-ansible` user.
* `SSH_PRIVATE_KEY`: A private key for a user on ops.gitlab.net to allow Terraform to clone modules hosted there.
* `SSH_KNOWN_HOSTS`: A standard `known_hosts` file (see `man 8 sshd`). It contains the public key of ops.gitlab.net for the same purpose of `SSH_PRIVATE_KEY`.

## Working locally

### Setup

#### Install Dependencies

1. To run both Terraform and Ansible locally you must install [asdf](https://github.com/asdf-vm/asdf) to ensure you have the correct version of Python and Terraform installed locally.
2. Python dependencies are managed through `pipenv`, after python is installed via `asdf`, install it first by running `pip install pipenv`.
3. From the root of the repository, run `pipenv shell` to launch a new shell in a virtualenv and `pipenv install --dev` to install all dependencies.
4. Run `ansible-galaxy collection install -r requirements.yml && ansible-galaxy role install -r requirements.yml` in the virtuenv to install Ansible dependencies.
5. After this, ensure you can run `ansible`. If not, you may need to run `asdf reshim python`.


#### Vendor GET

This project vendors Terraform modules and Ansible roles from [GET](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit/).

**We use the `db-provisioning` branch of GET until we are able to move to a GET official release**

To update the file to the latest on the `db-provisioning` branch run

```
make vendor
```

To add new files, Terraform modules, or Ansible roles to the `vendor/get` directory update the `vendor.cfg.yml` file.

### Setup Environment

Some environment variables need to be set for Ansible, Terraform and Chef. Copy the example environment variable and then source it:

```
cp private/<name>.env.example private/<name>.env
# Update paths to key locations
source private/<name>.env
```

### Terraform

**Note**: This project uses [GitLab managed Terraform state](https://docs.gitlab.com/ee/user/infrastructure/terraform_state.html).

1. Initialize Terraform state
```
cd terraform/environments/sandbox
 ../../../bin/terraform-init sandbox
```
2. Run plan
```
terraform plan -out sandbox.plan
```
3. Run apply
```
terraform apply sandbox.plan
```

### Ansible

1. Run `ansible-inventory` to check inventory for the environment

```
ansible-inventory -i inventory --graph
```
2. To configure a fleet run the corresponding playbook

```
ansible-playbook -i environments/sandbox/inventory consul.yml
```
## Project Workflow

**[CONTRIBUTING.md](CONTRIBUTING.md) is important, do not skip reading this.**
