from math import isnan, isinf
from ansible.utils.display import Display

display = Display()

def to_ruby(a):
    """Converts a Python value to its Ruby representation"""

    if isinstance(a, str):
        return a.__repr__()

    if isinstance(a, bool):
        if a:
            return "true"
        else:
            return "false"

    if a is None:
        return "nil"

    if isinstance(a, int):
        return a

    if isinstance(a, float):
        if isnan(a):
            return "Float::NAN"

        if isinf(a):
            ret = "Float::INFINITY"
            if a > 0:
                return ret
            else:
                return "-" + ret

    if isinstance(a, list):
        entries = []

        for i in a:
            entries.append(to_ruby(i))

        return "[{}]".format(", ".join(entries))


    if isinstance(a, dict):
        entries = []

        for k, v in a.items():
            entries.append("{}: {}".format(to_ruby(k), to_ruby(v)))

        return "{{{}}}".format(", ".join(entries))

    # Catch-all
    display.warning("to_ruby: Couldn't convert this value {} of type {}".format(a, type(a)))
    return a.__repr__()

class FilterModule(object):
    """Register to_ruby filter"""

    def filters(self):
        return {'to_ruby': to_ruby}
