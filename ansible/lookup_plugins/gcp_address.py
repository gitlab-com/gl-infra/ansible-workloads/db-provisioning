DOCUMENTATION = """
  lookup: gcp_address
  author: Ahmad Sherif <ahmad@gitlab.com>
  version_added: "2.10"
  short_description: Looks up a GCP address
  description:
      - Returns GCP addresses by their names
  options:
    _terms:
      description: Address name(s)
      required: True
    region:
      description: The region in which the address exists
      required: True
  notes:
    - Requires GOOGLE_APPLICATION_CREDENTIALS to be set in the environment
"""

EXAMPLES = """
---
- hosts: 127.0.0.1
  tasks:
    - set_fact: address="{{ lookup('gcp_address', 'load-balancer-address', region='us-east1') }}"
    - debug: msg="var is {{ address }} "
"""

RETURN = """
  _list:
    description: requested value/s
"""

from google.cloud import compute
from ansible.plugins.lookup import LookupBase

class LookupModule(LookupBase):
    def run(self, terms, region, variables=None):
        results = []

        project = variables['project_name']
        client = compute.AddressesClient()

        for term in terms:
            results.append(client.get(project=project, address=term, region=region).address)

        return results
