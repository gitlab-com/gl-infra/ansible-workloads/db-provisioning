DOCUMENTATION = """
  lookup: gsm
  author: John Jarvis <jjarvis@gitlab.com>
  version_added: "2.9"
  short_description: Looks up a GSM secret
  description:
      - Returns secrets that have been added to Google Secret Manager
  options:
    _terms:
      description: Name of the secret(s)
      required: True
    version:
      description: Version of the secret to fetch
      default: latest
      type: string
  notes:
    - Requires GOOGLE_APPLICATION_CREDENTIALS to be set in the environment
"""

EXAMPLES = """
---
- hosts: 127.0.0.1
  tasks:
    - set_fact: foo_password="{{ lookup('gsm', 'some_password') }}"
    - debug: msg="var is {{ foo_password }} "

    - set_fact: foo_password="{{ lookup('gsm', 'some_password', version=10) }}"
    - debug: msg="var at version 10 is {{ foo_password }} "

    - name: lookup password in loop
      debug: msg="{{ item }}"
      with_gsm:
        - some_password
        - some_other_password
"""

RETURN = """
  _list:
    description: requested value/s
"""

from ansible.errors import AnsibleError
from ansible.plugins.lookup import LookupBase
from ansible.utils.display import Display
from google.cloud import secretmanager
from google.api_core import exceptions
from os import getenv
import json
from json import JSONDecodeError

display = Display()


class LookupModule(LookupBase):
    def run(self, terms, variables=None, version="latest"):

        """
        :arg terms: a list of lookups to run.
            e.g. ['secret_name', 'another_secret' ]
        :kwarg variables: ansible variables active at the time of the lookup
        :kwarg version: secret version to fetch, defaults to "latest"
        """
        result = []

        display.v(f"version={version}")

        display.v(f"Looking up secret for {terms}")

        gcreds_fname = getenv("GOOGLE_APPLICATION_CREDENTIALS")
        if not gcreds_fname:
            raise AnsibleError(
                "You must set GOOGLE_APPLICATION_CREDENTIALS in the environment"
                " to use the Google Secret Manager lookup plugin"
            )

        project = self._project_from_creds(gcreds_fname)

        client = secretmanager.SecretManagerServiceClient()

        for term in terms:
            name = client.secret_version_path(project, term, version)
            display.v(f"Accessing secret {name}")
            try:
                response = client.access_secret_version(name=name)
                result.append(response.payload.data.decode("UTF-8"))
            except exceptions.PermissionDenied:
                raise AnsibleError(f"Unable to lookup secret {name}, does it exist?")

        return result

    def _project_from_creds(self, fname):
        with open(fname) as f:
            try:
                return json.load(f)["project_id"]
            except FileNotFoundError:
                raise AnsibleError(
                    f"Unable to open credential file {fname}, does it exist?"
                )
            except JSONDecodeError:
                raise AnsibleError(
                    f"Unable to decode credential file {fname}, is it JSON?"
                )
            except KeyError:
                raise AnsibleError(
                    f"project_id key does not exist in credential file {fname}"
                )
