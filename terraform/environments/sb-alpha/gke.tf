# Only create the GKE cluster in the sb-alpha until
# it is fully configured for monitoring

module "gke" {
  project            = var.project
  environment        = var.environment
  region             = var.region
  service_account_ci = var.service_account_ci
  source             = "../../modules/gke"
}

module "helm" {
  project = var.project
  source  = "../../modules/helm"
  depends_on = [
    module.gke
  ]
}
