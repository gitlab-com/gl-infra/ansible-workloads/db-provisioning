terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
    cloudflare = {
      source = "cloudflare/cloudflare"
      version = "= 3.1.0"
    }
    google = {
      source = "hashicorp/google"
      version = "= 3.90.0"
    }
    google-beta = {
      source = "hashicorp/google-beta"
    }
    local = {
      source = "hashicorp/local"
    }
    random = {
      source = "hashicorp/random"
    }
    helm = {
      source = "hashicorp/helm"
      version = "2.2.0"
    }
  }
  required_version = ">= 0.13"
}
