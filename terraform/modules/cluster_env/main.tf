module "consul" {
  prefix                = var.environment
  node_type             = "consul"
  node_count            = var.consul_node_count
  machine_type          = var.consul_machine_type
  machine_image         = var.machine_image
  disk_size             = var.default_disk_size
  disk_type             = var.default_disk_type
  disks                 = var.consul_disks
  service_account_roles = var.service_account_roles
  setup_external_ip     = false
  source                = "../get/gitlab_gcp_instance"
  vpc                   = var.vpc
  subnet                = var.subnet
  additional_labels = {
    "gitlab_env" : var.environment,
  }
}

resource "google_compute_firewall" "bastion-ssh" {
  name    = format("%v-%v", "bastion", var.environment)
  network = var.vpc

  allow {
    protocol = "tcp"
    ports    = [22]
  }

  source_ranges = ["0.0.0.0/0"]

  target_tags = ["bastion"]
}

locals {
  bastion_zones = [for i in range(var.bastion_node_count) : element(data.google_compute_zones.available.names, i)]
}

module "bastion" {
  prefix     = var.environment
  node_type  = "bastion"
  node_count = var.bastion_node_count

  machine_type  = var.bastion_machine_type
  machine_image = var.machine_image
  disk_size     = var.default_disk_size
  disk_type     = var.default_disk_type
  source        = "../get/gitlab_gcp_instance"
  vpc           = var.vpc
  subnet        = var.subnet
  zones         = local.bastion_zones
  setup_external_ip = false
  additional_labels = {
    "gitlab_env" : var.environment,
  }
}

module "bastion-backend-service" {
  name                   = "bastion"
  environment            = var.environment
  health_check           = "tcp"
  instance_self_links    = module.bastion.self_links
  instance_zones         = local.bastion_zones
  region                 = var.region
  service_port           = 22
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/backend-service.git?ref=v1.0.0"
}

module "bastion-lb" {
  backend_service        = module.bastion-backend-service.google_compute_region_backend_service_self_link
  environment            = var.environment
  forwarding_port_ranges = ["22"]
  health_check_ports     = ["80"]
  instances              = module.bastion.self_links
  lb_count               = "1"
  name                   = "gcp-tcp-lb-bastion"
  names                  = ["ssh"]
  project                = var.project
  region                 = var.region
  network                = var.vpc
  session_affinity       = "CLIENT_IP"
  source                 = "../../modules/tcp-lb"
  targets                = ["bastion"]
}

module "pubsub" {
  source        = "../pubsub"
  environment   = var.environment
  project       = var.project
  pubsub_topics = var.pubsub_topics
}

module "postgres-backup" {
  environment    = var.environment
  backup_readers = []
  backup_writers = ["projectEditor:${var.project}"]
  kms_key_id     = ""
  source         = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/database-backup-bucket.git?ref=v5.0.1"
}
