variable "environment" {}
variable "project" {}
variable "region" {}
variable "service_account_ci" {}
variable "vpc" {}
variable "subnet" {}

variable "service_account_roles" { default = ["roles/editor"] }

variable "object_storage_buckets" {
  default = []
}

variable "consul_machine_type" {
  default = "n1-standard-4"
}

variable "consul_node_count" {
  default = 3
}

variable "consul_disks" {
  default = [
    {
      size        = 50
      type        = "pd-standard"
      device_name = "log"
    },
  ]
}

variable "default_disk_size" { default = "100" }
variable "default_disk_type" { default = "pd-standard" }

variable "bastion_node_count" {
  default = 1
}

variable "bastion_machine_type" {
  default = "n1-standard-4"
}

variable "machine_image" { default = "ubuntu-1804-lts" }

variable "pubsub_topics" {
  default = ["system"]
}
