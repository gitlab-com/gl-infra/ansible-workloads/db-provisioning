
module "postgres" {
  name_override         = "${var.environment}-postgres-${var.name}"
  prefix                = var.name
  node_type             = "postgres"
  node_count            = var.postgres_node_count
  machine_type          = var.postgres_machine_type
  machine_image         = var.machine_image
  disk_size             = var.default_disk_size
  disk_type             = var.default_disk_type
  disks                 = var.postgres_disks
  service_account_roles = var.service_account_roles
  setup_external_ip     = false
  source                = "../get/gitlab_gcp_instance"
  vpc                   = var.vpc
  subnet                = var.subnet
  additional_labels = {
    "gitlab_shard" : var.name,
    "gitlab_env" : var.environment,
  }
}

module "secrets" {
  source             = "../secrets"
  environment        = var.environment
  name               = var.name
  secrets            = var.secrets
  project            = var.project
  service_account_ci = var.service_account_ci
}

module "pgbouncer" {
  source = "../pgbouncer_cluster"

  prefix      = var.name
  name        = "pgbouncer"
  node_count  = var.pgbouncer_node_count
  region      = var.region
  environment = var.environment
  project     = var.project
  vpc         = var.vpc
  subnet      = var.subnet

  machine_type  = var.pgbouncer_machine_type
  machine_image = var.machine_image
  disks         = var.pgbouncer_disks

  geo_site       = var.geo_site
  geo_deployment = var.geo_deployment
}

module "pgbouncer-sidekiq" {
  source = "../pgbouncer_cluster"

  prefix      = var.name
  name        = "pgbouncer-sidekiq"
  node_count  = var.pgbouncer_node_count
  region      = var.region
  environment = var.environment
  project     = var.project
  vpc         = var.vpc
  subnet      = var.subnet

  machine_type  = var.pgbouncer_machine_type
  machine_image = var.machine_image
  disks         = var.pgbouncer_disks

  geo_site       = var.geo_site
  geo_deployment = var.geo_deployment
}
