output "cluster_endpoint" {
  value = module.gitlab-gke.cluster_endpoint
}

output "access_token" {
  value = data.google_service_account_access_token.kubernetes_sa.access_token
}

output "cluster_ca_certificate" {
  value = module.gitlab-gke.cluster_ca_certificate
}
