resource "helm_release" "consul" {
  name = "consul"

  repository       = "https://helm.releases.hashicorp.com"
  chart            = "consul"
  version          = "0.33.0"
  namespace        = "consul"
  create_namespace = true
  wait             = false

  # The `consul-consul-client` service account is the Kubernetes service account
  # created by the Chart.
  # Workload Identity is required to give consul pods the permission to list instances,
  # we setup workload identity for `consul-consul-client` in the GKE module.
  values = [
    <<-EOT
      global:
        enabled: false
        datacenter: gitlab_consul
      client:
        enabled: true
        # join the gitlab_node_type=consul nodes outside of k8s
        join: ["provider=gce tag_value=consul"]
        serviceAccount:
          annotations: |
            iam.gke.io/gcp-service-account: consul-consul-client@${var.project}.iam.gserviceaccount.com
    EOT
  ]
}

resource "kubernetes_service" "consul_http" {
  metadata {
    name      = "gitlab-consul"
    namespace = "consul"
  }

  spec {
    selector = {
      app     = "consul"
      release = "consul"
    }
    type = "ClusterIP"
    port {
      name        = "http"
      port        = 8500
      protocol    = "TCP"
      target_port = "http"
    }
  }

  depends_on = [
    helm_release.consul
  ]
}
