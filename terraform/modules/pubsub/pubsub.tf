locals {
  full_topic_names = toset(formatlist(
    "%v-%v-%v-%v",
    "pubsub",
    var.pubsub_topics,
    "inf",
    var.environment,
  ))
}

resource "google_pubsub_topic" "pubsub_topics" {
  for_each = local.full_topic_names

  project = var.project
  name    = each.value
}
