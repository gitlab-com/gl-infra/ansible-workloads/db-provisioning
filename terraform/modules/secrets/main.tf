locals {
  secret_prefix = var.name == "" ? "${var.environment}" : "${var.environment}-${var.name}"
}

resource "random_password" "placeholder" {
  length           = 16
  special          = true
  override_special = "_%@"
}

resource "google_secret_manager_secret" "gitlab" {
  count     = length(var.secrets)
  secret_id = "${local.secret_prefix}-${var.secrets[count.index]}"

  replication {
    automatic = true
  }
}

resource "google_secret_manager_secret_version" "gitlab" {
  count       = length(var.secrets)
  secret      = google_secret_manager_secret.gitlab[count.index].id
  secret_data = "${var.secrets[count.index]}!${random_password.placeholder.result}"
}

resource "google_secret_manager_secret_iam_member" "gitlab" {
  count     = length(var.secrets)
  secret_id = google_secret_manager_secret.gitlab[count.index].id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${var.service_account_ci}@${var.project}.iam.gserviceaccount.com"
}
