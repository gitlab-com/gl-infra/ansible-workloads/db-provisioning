variable "service_account_ci" {}
variable "environment" {}
variable "project" {}
variable "secrets" {
  default = []
}
variable "name" {
  default = ""
}
