# GitLab.com tcp-lb Terraform Module

This is a copy of https://ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb/
until support is added to GET in https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit/-/issues/221

We are copying the module to override lifecycle protection, which cannot be parameterized
https://github.com/hashicorp/terraform/issues/3116

## Contributing

Please see [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
