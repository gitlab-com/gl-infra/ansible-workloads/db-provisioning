variable "lb_count" {
}

# list of fqdns for this lb
variable "fqdns" {
  type    = list(string)
  default = []
}

variable "network" {
  default = null
}

# These should be set for an internal load balancer
variable "subnetwork_self_link" {
  default = ""
}

variable "ip_cidr_range" {
  default = "10.0.0.0/18"
}

variable "vpc" {
  default = ""
}

variable "backend_service" {
  default = ""
}

variable "spectrum_config" {
  default = null
}

#####################################

variable "load_balancing_scheme" {
  type    = string
  default = "EXTERNAL"
}

variable "health_check_ports" {
  type = list(string)
}

variable "health_check_request_paths" {
  type = list(string)

  default = []
}

variable "forwarding_port_ranges" {
  type = list(string)
}

variable "gitlab_zone" {
  default = null
}

variable "instances" {
  type = list(string)
}

variable "targets" {
  type        = list(string)
  description = "target tags for the load balancer"
}

variable "environment" {
  type        = string
  description = "The environment name"
}

variable "name" {
}

variable "names" {
  type        = list(string)
  description = "Names for the lbs"
}

variable "project" {
  type        = string
  description = "The project name"
}

variable "region" {
  type        = string
  description = "The target region"
}

variable "session_affinity" {
  type        = string
  description = "Load balancer session affinity"
  default     = "NONE"
}
