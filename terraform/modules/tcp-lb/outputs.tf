output "ip_address" {
  value = length(google_compute_address.default) > 0 ? google_compute_address.default[0].address : null
}
