locals {
  pgbouncer_nodes = [for i in range(var.node_count) : element(data.google_compute_zones.available.names, i)]
}
# name=type
# prefix=shard
module "instances" {
  source        = "../get/gitlab_gcp_instance"
  name_override = "${var.environment}-${var.name}-${var.prefix}"
  prefix        = var.prefix
  node_type     = var.name
  node_count    = var.node_count
  tags          = [var.name]
  zones         = local.pgbouncer_nodes

  machine_type  = var.machine_type
  machine_image = var.machine_image
  disks         = var.disks
  vpc           = var.vpc
  subnet        = var.subnet

  geo_site          = var.geo_site
  geo_deployment    = var.geo_deployment
  setup_external_ip = false
  additional_labels = {
    "gitlab_shard" : var.prefix,
    "gitlab_env" : var.environment,
  }
}

module "backend_service" {
  name                   = "${var.name}-${var.prefix}"
  backend_service_type   = "regional"
  create_backend_service = true
  environment            = var.environment
  health_check           = "tcp"
  instance_self_links    = module.instances.self_links
  instance_zones         = local.pgbouncer_nodes
  region                 = var.region
  service_port           = 6432
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/backend-service.git?ref=v1.0.0"
}

module "internal_lb" {
  backend_service        = module.backend_service.google_compute_region_backend_service_self_link
  environment            = var.environment
  forwarding_port_ranges = ["6432"]
  health_check_ports     = ["6432"]
  instances              = module.instances.self_links
  lb_count               = "1"
  load_balancing_scheme  = "INTERNAL"
  name                   = "${var.name}-${var.prefix}-ilb"
  names                  = [var.name]
  network                = var.vpc
  project                = var.project
  region                 = var.region
  source                 = "../../modules/tcp-lb"
  subnetwork_self_link   = var.subnet
  targets                = [var.name]
}
